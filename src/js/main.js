import Person from './Person';
import '../styles/main.less';
// eslint-disable-next-line no-undef
const $ = require('jquery');

function fetchData(url) {
  return fetch(url)
    .then(response => response.json())
    .catch(error => error);
}

const URL = 'http://localhost:3000/person';
// eslint-disable-next-line no-unused-vars
const person = new Person();
fetchData(URL)
  .then(result => {
    person.name = result.name;
    $('.name-age-info').html(
      `MY NAME IS ${result.name} ${result.age}YO AND THIS IN MU RESUME/CV`
    );
    $('.description').html(result.description);
    $.each(result.educations, (index, data) => {
      $('ul').append(`
                    <li class="fistList">
                        <span>${data.year}</span>
                    </li>
                    <li class="secondList">
                        <span>${data.title}</span>      
                    </li>
                    <li class="lastList">
                         <p>${data.description}</p>
                    </li>
                </tr>`);
    });
  })
  .catch(error => error);
